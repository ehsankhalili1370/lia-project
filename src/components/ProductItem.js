import React from 'react';
import { getCommaSeprated } from '../utils/helper';

const ProductItem = ({ product, addToBasket }) => {
   return (
      <div className="product-item">
         <img alt="product item" src={`https://back.liateam.com${product.small_pic}`} />

         <div className="is-new">{product.isNew && <div>جدید</div>}</div>

         <div className="title">{product.title}</div>
         <small className="volume">{product.volume}</small>

         <div className="detail">
            <div onClick={() => addToBasket(product)}>
               <i className="fa fa-cart-plus" aria-hidden="true"></i>
            </div>

            <div className="price">{getCommaSeprated(product.price.price)} تومان</div>
         </div>
      </div>
   );
};

export default ProductItem;
