import '../styles/main.scss';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import CategoryList from './pages/CategoryList';
import ProductList from './pages/ProductList';
import Header from './Header';

const App = () => {
   return (
      <div>
         <BrowserRouter>
            <Header />
            <Switch>
               <Route path="/" exact component={CategoryList} />
               <Route path="/categories/:id" exact component={ProductList} />
            </Switch>
         </BrowserRouter>
      </div>
   );
};

export default App;
