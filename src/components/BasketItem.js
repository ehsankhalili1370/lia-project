import React from 'react';
import { getCommaSeprated } from '../utils/helper';

const BasketItem = ({ product, addToBasket, reduceFromBasket, removeFromBasket }) => {
   return (
      <div className="basket-item">
         <div>
            <img alt="basket item" src={`https://back.liateam.com${product.small_pic}`} />
         </div>
         <div className="detail">
            {product.title}

            <div className="price">{getCommaSeprated(product.price.price)} تومان</div>

            <div className="remove-button">
               <i className="fa fa-times" aria-hidden="true" onClick={() => removeFromBasket(product)}></i>
            </div>

            <div className="actions">
               <span onClick={() => addToBasket(product)}>+</span>
               {product.count}
               <span onClick={() => reduceFromBasket(product)}> - </span>
            </div>
         </div>
      </div>
   );
};

export default BasketItem;
