import React from 'react';
import { Link } from 'react-router-dom';

const CategotyItem = ({ category }) => {
   return (
      <Link to={`/categories/${category.id}`} className="category-item">
         <img alt="category" src={`http://back.dev.liateam.ir/${category.image}`} />
         <div>
            {category.name}

            <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
         </div>
      </Link>
   );
};

export default CategotyItem;
