import '../../styles/categories.scss';
import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchCategories } from '../../actions/categoryAction';
import CategotyItem from '../CategotyItem';

const renderList = categories => {
   return categories.map(category => <CategotyItem key={category.id} category={category} />);
};

const CategoryList = props => {
   useEffect(() => {
      props.fetchCategories();
   }, []);

   const { categories } = props;

   return (
      <div className="categories-container">
         <h2>دسته بندی</h2>
         {categories.length === 0 ? (
            <div>Loading ...</div>
         ) : (
            <div className="categories-list">{renderList(categories)}</div>
         )}
      </div>
   );
};

const mapDispatchToProps = dispatch => {
   return {
      fetchCategories: bindActionCreators(fetchCategories, dispatch),
   };
};

const mapStateToProps = state => {
   return {
      categories: Object.values(state.categories),
   };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryList);
