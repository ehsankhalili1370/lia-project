import '../../styles/products.scss';
import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchProducts, clearProducts } from './../../actions/productAction';
import { addToBasket } from './../../actions/basketAction';
import ProductItem from '../ProductItem';

const renderPagination = props => {
   const {
      products: { page, pageCount },
      fetchProducts,
   } = props;
   const { id } = props.match.params;

   if (!pageCount) return;

   let content = [];

   for (let index = 0; index < pageCount; index++) {
      const className = index + 1 === page * 1 ? 'active' : '';

      content.push(
         <div key={index} onClick={() => fetchProducts(id, index + 1)} className={className}>
            {index + 1}
         </div>
      );
   }

   return <div className="pagination-container">{content}</div>;
};

const renderList = (products, addToBasket) => {
   return products.list.map(product => <ProductItem key={product.id} product={product} addToBasket={addToBasket} />);
};

const ProductList = props => {
   const { products, match, fetchProducts, clearProducts, addToBasket } = props;

   useEffect(() => {
      const { id } = match.params;

      fetchProducts(id);

      return () => {
         clearProducts();
      };
   }, []);

   return (
      <div className="products-container">
         <h2>محصولات</h2>

         {products.list.length === 0 ? (
            <div>Loading ...</div>
         ) : (
            <div className="products-list">{renderList(products, addToBasket)}</div>
         )}

         {renderPagination(props)}
      </div>
   );
};

const mapStateToProps = state => {
   return {
      products: state.products,
   };
};

const mapDispatchToProps = dispatch => {
   return {
      fetchProducts: bindActionCreators(fetchProducts, dispatch),
      clearProducts: bindActionCreators(clearProducts, dispatch),
      addToBasket: bindActionCreators(addToBasket, dispatch),
   };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
