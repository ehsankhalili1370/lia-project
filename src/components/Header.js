import '../styles/header.scss';
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addToBasket, reduceFromBasket, removeFromBasket } from '../actions/basketAction';
import BasketItem from './BasketItem';
import { getCommaSeprated } from '../utils/helper';

const renderBasketModal = props => {
   const { basket, addToBasket, reduceFromBasket, removeFromBasket } = props;
   if (!basket.length) {
      return (
         <div className="basket-modal">
            <div className="basket-item">سبد خرید شما خالی است</div>
         </div>
      );
   }

   let totalPrice = 0;
   basket.forEach(product => (totalPrice += product.price.final_price * product.count));

   return (
      <div className="basket-modal">
         {basket.map(product => (
            <BasketItem
               key={product.id}
               product={product}
               addToBasket={addToBasket}
               reduceFromBasket={reduceFromBasket}
               removeFromBasket={removeFromBasket}
            />
         ))}

         <div className="basket-item summary">
            <div>
               جمع کل:
               <br />
               <br />
               مبلغ قابل پرداخت:
            </div>
            <div>
               {getCommaSeprated(totalPrice)} تومان
               <br />
               <br />
               {getCommaSeprated(totalPrice)} تومان
            </div>
         </div>
      </div>
   );
};

const Header = props => {
   const [modalVisible, setModalVisible] = useState(false);

   let counter = 0;

   props.basket.forEach(product => {
      counter += product.count;
   });

   useEffect(() => {
      document.addEventListener('click', e => {
         const basketContainer = document.querySelector('.basket-container');
         const basketModal = document.querySelector('.basket-modal');

         if (basketModal && !basketContainer.contains(e.target) && document.body.contains(e.target)) {
            setModalVisible(false);
         }
      });
   }, []);

   return (
      <header>
         <div className="main-header">
            <div>
               <img width={50} alt="lia logo" src="/img/logo.png" />
               <Link to="/">خانه</Link>
            </div>
            <div>رضا پورجباری عزیز</div>
         </div>
         <div className="second-header">
            <div className="links">
               <span>مراقبا پوست</span>
               <span>مراقبا مو</span>
               <span>مراقبا بدن</span>
               <span>آرایشی</span>
               <span>پرفروش ترین</span>
               <span>جدید ترین</span>
            </div>

            <div className="basket-container">
               <div onClick={() => setModalVisible(!modalVisible)}>
                  <i className="fa fa-shopping-cart" aria-hidden="true"></i>

                  {props.basket.length > 0 && <div className="counter">{counter}</div>}
               </div>

               {modalVisible && renderBasketModal(props)}
            </div>
         </div>
      </header>
   );
};

const mapStateToProps = state => {
   return {
      basket: Object.values(state.basket),
   };
};

const mapDispatchToProps = dispatch => {
   return {
      addToBasket: bindActionCreators(addToBasket, dispatch),
      reduceFromBasket: bindActionCreators(reduceFromBasket, dispatch),
      removeFromBasket: bindActionCreators(removeFromBasket, dispatch),
   };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
