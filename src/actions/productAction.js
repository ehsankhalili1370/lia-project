import server from '../api/server';
import { FETCH_PRODUCTS, CLEAR_PRODUCTS } from './types';

export const fetchProducts = (id, page) => async dispatch => {
   const pageNumber = page || 1;
   const response = await server.get(`/get_product?categories=${id}&page=${pageNumber}`);

   dispatch({ type: FETCH_PRODUCTS, payload: response.data });
};

export const clearProducts = () => {
   return { type: CLEAR_PRODUCTS };
};
