import { ADD_PRODUCT_TO_BASKET, REDUCE_PRODUCT_FROM_BASKET, REMOVE_FROM_BASKET } from './types';

export const addToBasket = product => {
   return { type: ADD_PRODUCT_TO_BASKET, payload: product };
};

export const reduceFromBasket = product => {
   return { type: REDUCE_PRODUCT_FROM_BASKET, payload: product };
};

export const removeFromBasket = product => {
   return { type: REMOVE_FROM_BASKET, payload: product };
};
