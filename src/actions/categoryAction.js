import server from '../api/server';
import { FETCH_CATEGORIES } from './types';

export const fetchCategories = () => async dispatch => {
   const response = await server.get('/get_categories');

   dispatch({ type: FETCH_CATEGORIES, payload: response.data });
};
