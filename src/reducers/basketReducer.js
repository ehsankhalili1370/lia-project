import _ from 'lodash';
import { ADD_PRODUCT_TO_BASKET, REDUCE_PRODUCT_FROM_BASKET, REMOVE_FROM_BASKET } from '../actions/types';

export default (state = {}, action) => {
   switch (action.type) {
      case ADD_PRODUCT_TO_BASKET: {
         let count = state[action.payload.id] ? state[action.payload.id].count + 1 : 1;
         return { ...state, [action.payload.id]: { ...action.payload, count } };
      }
      case REDUCE_PRODUCT_FROM_BASKET: {
         let count = state[action.payload.id] ? state[action.payload.id].count - 1 : 0;
         if (count > 0) return { ...state, [action.payload.id]: { ...action.payload, count } };
         return _.omit(state, action.payload.id);
      }
      case REMOVE_FROM_BASKET:
         return _.omit(state, action.payload.id);
      default:
         return state;
   }
};
