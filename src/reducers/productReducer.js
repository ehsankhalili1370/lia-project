import { FETCH_PRODUCTS, CLEAR_PRODUCTS } from '../actions/types';

const INITIAL_STATE = {
   list: [],
   page: 1,
   pageCount: null,
};

export default (state = INITIAL_STATE, action) => {
   switch (action.type) {
      case FETCH_PRODUCTS:
         return {
            ...state,
            list: [...action.payload.list],
            page: action.payload.page,
            pageCount: action.payload.pagecount,
         };

      case CLEAR_PRODUCTS:
         return { ...INITIAL_STATE };

      default:
         return state;
   }
};
